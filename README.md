# WD-40 For Chests

This removes the squeaking sound from chests.


## [Video Demonstration](https://www.youtube.com/watch?v=GcuGvYrOizE "youtube")

---
The source here is for version 1.19.2. Releases will have 1.19.x - 1.20.x and will continue to be updated as new major versions get released.

---
I am not affiliated in any way with the WD-40 company, or any of its products. WD-40 is used as a fun way to describe the removal of the squeaking in the default chest opening/closing sound.

